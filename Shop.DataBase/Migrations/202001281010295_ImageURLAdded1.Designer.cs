// <auto-generated />
namespace Shop.DataBase.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class ImageURLAdded1 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ImageURLAdded1));
        
        string IMigrationMetadata.Id
        {
            get { return "202001281010295_ImageURLAdded1"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
