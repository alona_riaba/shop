﻿using Shop.DataBase;
using Shop.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Services
{
    public class ConfigurationService
    {

        //public static ConfigurationService ClassObject {
        //    get
        //    {
        //        if (privateInMemoryObject == null)
        //            privateInMemoryObject = new ConfigurationService();

        //        return privateInMemoryObject;
        //    }
        //}
        //private static ConfigurationService privateInMemoryObject { get; set; }

        //private ConfigurationService()
        //{
            
        //}


        public Config GetConfig(string Key)
        {
            using (var context = new SHContext())
            {
                return context.Configs.Find(Key);
            }
        }
    }
}
